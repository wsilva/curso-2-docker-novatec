
# Introdução

## Início

O primeiro anúncio público do docker foi feito por Solomon Hykes, CEO da dotCloud em 15 de março de 2013 numa lightning talk durante uma conferência de Python na Califórnia.

O vídeo da mini palestra ainda está disponível no [Youtube](https://youtu.be/wW9CAH9nSLs) - https://youtu.be/wW9CAH9nSLs

![Solomon Hykes](./img/solomon_hykes.jpg)

 Inicialmente o Docker foi inventado para que a dotCloud pudesse suportar a gerencia de seu PAAS, onde desenvolvedores poderiam fazer o deploy de suas aplicações de uma maneira similar ao Heroku mas pode debaixo dos panos tudo rodaria em containers.

## O que é o Docker

![Docker Logo](./img/docker.png)

"Docker allows you to package an application with all of its dependencies into a standardized unit for software development"

Pela definição da própria Docker parece ser uma Virtual Machine mas não é.

### Virtualização

Na virtualização por software temos um sistema operacional inteiro chamado de Guest com seu kernel, bibliotecas e binários rodando sobre um outro sistema operacional chamado de Host com a ajuda de um Hypervisor (Virtualbox, VMWare, Xen, Kvm, etc.)

*Na virtualização por hardware funciona um pouco diferente mas o sistema Guest todo roda sobre o host.

![VMs Stack](./img/virtualizacao-stack.png)

### Containers/Docker

Containers são como os sistemas operacionais guests mas eles compartilham recursos como o Kernel e bibliotecas do sistema operacional Host rodando como um processo único

![Docker Stack](./img/docker-stack.png)

Quem faz esse meio campo é o Docker Engine

### Vantagens

 - Padronização de ambientes;
 - Melhora a utilização de recursos físicos de infraestrutura;
 - Fácil a recuperação de dados (imagem);
 - Reutilizável (copy-on-write);
 - Faz o isolamento utilizando namespaces (isola recursos como processos pid e comunicações interprocessos, rede, filesystem, etc);
 - Inicia muito rápido (inicia só o processo e não a stack toda);
 - Fácil distribuição (registry / docker hub);
 - Limitação de memória (podemos designar o limite de memória do container);
 - Build automático (com Dockerfiles);
 - Engine fornece uma API;
 - Sempre em inovação (quase que 1 release por mês);


### Desvantagens
 - Overhead de IO;
 - Se o dockerhost cai, todos os containers nele caem também;
 - Dificulta troubleshooting (mais uma camada na investigação);
 - Por mais isolado que esteja o processo por causa dos namespaces como há compartilhamento de recursos há a possibilidade de um ataque sofisticado, ou a exploração de uma configuração falha.
 - Menos portabilidade. (Apenas containers Linux em Hosts Linux)(*);

 (*) A Microsoft com a ajuda da Docker vai disponibilizar no Windows Server 2016 a possibilidade de rodar containers Windows em Hosts Windows.

## Resumindo

O Docker é praticamente um ecossistema, opensource desde o release 0.9 (março de 2013) hoje é composto pelos seguintes componentes:

### Containers
Os Docker containers são os antigos LXC (Linux Containers) que existem em toda distribuição Linux desde o Kernel 2.6.26 (distribuído em julho de 2008) sendo gerenciados por uma docker engine.

### Engine
Essa engine é um daemon que sobe os container, faz o trabalho de criar o CHROOT e gerencia os recursos de rede, de processador, de memória e demais recursos que os containers utilizam.

### Cliente
A Engine expoe uma API onde com um client conseguimos passar os comandos para criar, executar, parar, remover, adicionar processos, listar imagens e listar containers entre outros comandos.

### Registry
Quando salvamos o estado de um container geramos uma imagem que podemos reutilizar em outro container através de uma técnica já conhecida chamada copy-on-write, falaremos mais sobre depois.

O Registry também chamado de Docker Hub é um repositório público similar ao github onde hospedamos as imagens.

### Arquitetura
Podemos representar a arquitetura do Docker com a figura:

![Arquitetura Docker](./img/docker-architecture.png)

Onde o Docker Host pode ser uma máquina física ou uma máquina virtual.

### Ferramentas de Orquestração
Algumas ferramentas para facilitar a disponibilização de aplicações lançadas pela Docker são o Compose, Machine e o Swarm.
