# 2º Curso Docker - Centro de Treinamento Novatec


## Sobre o curso

Neste curso abordaremos o que é o Docker, suas origens, suas vantagens e desvantagens, porque é uma grande quebra de paradigma.
Iniciaremos as partes práticas abordando a instalação, o funcionamento, rodaremos os primeiros exemplos começando pelo conhecido Hello World.
Avançaremos com a utilização de imagens públicas, montaremos nossas próprias imagens, disponibilizaremos no registry público, chamado de docker hub, e por fim abordaremos montagens automáticas de imagens.
Em seguida veremos mais como gerenciar containers e imagens. Inicialização, pausa, término, exportação/importação, salvar/carregar containers, compartilhar volumes com hosts, mapeamento de portas, definição de variáveis de ambiente e comunicação entre containers.
Na parte de orquestração veremos como montar e rodar uma aplicação que utiliza vários serviços como se ela estivesse em nuvem utilizadno docker-compose. Finalizaremos demonstrando como utilizar o docker-machine para provisionar VMs locais ou na nuvem e utilizaremos o docker-swarm para montar uma rede de hosts rodando docker onde podemos subir instâncias da nossa aplicação.


## Pré requisitos

Além de um acesso a internet é desejável um certo conhecimento de Linux (familiaridade com a command line interface - cli), noções básicas de Git, de virtualização e ter tido contato com alguma aplicação Web para acompanhar alguns exemplos.
No curso presencial também é desejável que o aluno traga seu computador (preferencialemnte notebook) para acompanhar os exemplos e exercícios.
Se possível seria interessante já possuir uma conta no github ou bitbucket, e uma conta no docker hub para agilizarmos este processo durante o curso.


## Instrutor

Meu nome é Wellington Figueira da Silva, técnico em telecomunicações, programador, atualmente no time de Devops da Global Fashion Group (Tricae, Kanui e Dafiti), participante em comunidades de PHP e Python, certificado ZCE Zend Certified Engineer e praticamente graduado em Sistemas de Informação pela USP.