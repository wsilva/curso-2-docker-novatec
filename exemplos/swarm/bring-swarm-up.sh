#!/bin/bash

docker-machine create \
    -d virtualbox \
    kv;

docker $(docker-machine config kv) run -d \
    -p "8500:8500" \
    -h "consul" \
    progrium/consul -server -bootstrap;

eval "$(docker-machine env kv)";
docker ps;


docker-machine create \
    -d virtualbox \
    --swarm --swarm-master \
    --swarm-discovery="consul://$(docker-machine ip kv):8500" \
    --engine-opt="cluster-store=consul://$(docker-machine ip kv):8500" \
    --engine-opt="cluster-advertise=eth1:2376" \
    abelha-rainha;

docker-machine create -d virtualbox \
    --swarm \
    --swarm-discovery="consul://$(docker-machine ip kv):8500" \
    --engine-opt="cluster-store=consul://$(docker-machine ip kv):8500" \
    --engine-opt="cluster-advertise=eth1:2376" \
  abelha-operaria-1;

eval $(docker-machine env --swarm abelha-rainha);

docker info;

docker network create \
    --driver overlay \
    --subnet=10.0.0.0/24 \
    rede-overlay;

docker network ls;

